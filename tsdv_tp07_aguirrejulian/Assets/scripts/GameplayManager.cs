﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    private GameOverZone gameOverZone;
    void Start()
    {
        gameOverZone = FindObjectOfType<GameOverZone>();
        gameOverZone.onWin = Pierdo;
    }

    void Pierdo()
    {
        GameManager.GetInstance().NextScene();
    }
}
