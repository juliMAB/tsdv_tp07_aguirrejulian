﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBricks : MonoBehaviour
{
    private Color[] aColors = {Color.red, Color.blue, Color.magenta, Color.yellow};
    [SerializeField] private GameObject SpawnBox;
    [SerializeField] private int  maxLarge;
    [SerializeField] private int maxHigh;
    void Start()
    {
        
        for (int i = 0; i < maxHigh; i++)
        {
            for (int j = 0; j < maxLarge; j++)
            {
                GameObject go = Instantiate(SpawnBox);
                //go = SpawnBox;
                go.transform.localScale = new Vector3(SpawnBox.transform.localScale.x / maxLarge,
                    SpawnBox.transform.localScale.y / maxHigh, SpawnBox.transform.localScale.z);
                go.transform.position += new Vector3(
                    SpawnBox.transform.position.x - (SpawnBox.transform.localScale.x/2) + (go.transform.localScale.x * j) + go.transform.localScale.x/2, 
                     - (SpawnBox.transform.localScale.y/2) + (go.transform.localScale.y * i) + go.transform.localScale.y/2,
                    SpawnBox.transform.position.z);
                go.GetComponent<MeshRenderer>().material.color = aColors[Random.Range(0, aColors.Length)];
                go.layer = LayerMask.NameToLayer("Bricks");
            }
        }
        Destroy(SpawnBox);
    }
}
