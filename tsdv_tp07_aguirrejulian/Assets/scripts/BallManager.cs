﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;

    [SerializeField] private Vector3 velocidad;
    private bool coliciono;

    void Start()
    {
        velocidad = Vector3.up * speed;
    }

    void Update()
    {
        transform.position += velocidad.normalized*Time.deltaTime*speed;
        coliciono = false;
    }

    float HitFactor(Vector3 ballPos, Vector3 padPos, float padWidth)
    {
        return (ballPos.x - padPos.x) / padWidth;
    }
    void OnCollisionEnter(Collision collision)
    {
        if (!coliciono)
        {
            if (collision.gameObject.tag == "Pad")
            {
                float x = HitFactor(transform.position, collision.transform.position, collision.collider.bounds.size.x);
                velocidad = new Vector3(x, 1, 0).normalized * speed;
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Bricks"))
            {
                if (Mathf.Abs(collision.transform.position.x - transform.position.x) < Mathf.Abs(collision.transform.position.y - transform.position.y))
                {
                    if (collision.transform.position.y > gameObject.transform.position.y)
                    {
                        velocidad.y = -Mathf.Abs(velocidad.y);
                    }
                    else if (collision.transform.position.y < gameObject.transform.position.y)
                    {
                        velocidad.y = Mathf.Abs(velocidad.y);
                    }
                }
                else
                {
                    velocidad.x *= -1;

                }
                Destroy(collision.gameObject);
                coliciono=true;
            }
            else if (collision.gameObject.layer == LayerMask.NameToLayer("Edges"))
            {
                if (Mathf.Abs(collision.contacts[0].point.x - transform.position.x) > Mathf.Abs(collision.contacts[0].point.y - transform.position.y))
                {
                    velocidad.x *= -1;
                }
                else
                {
                    velocidad.y *= -1;
                }
                coliciono = true;
            }
        }
        
    }
}


