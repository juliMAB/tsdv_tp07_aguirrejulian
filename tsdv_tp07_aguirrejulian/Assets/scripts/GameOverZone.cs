﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverZone : MonoBehaviour
{
    public delegate void OnWinDelegate();
    public OnWinDelegate onWin;

    public int vidas;
    public GameObject ballGameObject;
    public GameObject padGameObject;

    private Vector3 ballInitialPosition;
    private Vector3 padInitialPosition;
    void Start()
    {
        ballInitialPosition = ballGameObject.transform.position;
        padInitialPosition = padGameObject.transform.position;
    }

    private void OnTriggerEnter( Collider collision)
    {
        if (collision.transform.CompareTag("Ball"))
        {
            
            ballGameObject.transform.position = ballInitialPosition;
            padGameObject.transform.position = padInitialPosition;
            vidas--;
        }

        if (vidas <0)
        {
            onWin?.Invoke();
        }
    }
}
