﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public void NextScene()
    {
        GameManager.GetInstance().NextScene();
    }

    public void MenuScene()
    {
        GameManager.GetInstance().GoToMainMenu();
    }

    public void Exit()
    {
        GameManager.GetInstance().Exit();
    }
}
