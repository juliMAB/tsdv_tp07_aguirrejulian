﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private bool stopRight;
    [SerializeField] private bool stopLeft;

    void FixedUpdate()
    {
        stopRight = Physics.Raycast(transform.position, Vector3.right, transform.localScale.x/2);
        stopLeft = Physics.Raycast(transform.position, Vector3.left, transform.localScale.x/2);

    }

    void Update()
    {
        float a = Input.GetAxisRaw("Horizontal");
        if (a<0)
        {
            if (!stopLeft)
            {
                transform.position += Vector3.right * Time.deltaTime*speed*a;
            }
        }
        else if (a>0)
        {
            if (!stopRight)
            {
                transform.position += Vector3.right * Time.deltaTime * speed*a;
            }
        }
    }
}
